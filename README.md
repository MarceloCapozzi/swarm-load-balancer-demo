# Ejemplo de balanceador de carga HTTP utilizando SWARM

Con este ejemplo crearemos un balanceador de carga (traefik) para balancear la carga de peticiones entre *n* cantidad de webservers.

## Modo de uso

> Es necesario tener *swarm* previamente instalado y al menos un *manager* donde implementar este ejemplo.

- Descargar el repositorio

    ```
    git clone git@gitlab.com:MarceloCapozzi/swarm-load-balancer-demo.git
    cd swarm-load-balancer-demo
    ```

- Implementar el servicio

    ```
    docker stack deploy -c stack.yml demo
    ```

### Escalar servicio

- Escalar un servicio determinado

    Ejemplo para escalar a **10** las instancias del webserver:

    ```
    docker service scale demo_webserver=10
    ```

### Verificar el estado del  servicio

- Listar los servicios

    ```
    docker service ls
    ```
    Ejemplo:

     ![output docker service ls!](/images/docker-service-ls.jpg "output docker service ls")

- Listar las tareas de uno o mas servicios

    ```
    docker service ps xxx yyy
    ```

    | referencia | valor |
    | --- | --- |
    | xxx | opciones |
    | yyy | servicio o servicios |

    Ejemplo al listar tareas de un servicio (*loadbalancer*):

     ![output docker service ps service!](/images/docker-service-ps-loadbalancer.jpg "output docker service ps service")

    Ejemplo al listar tareas uno o más servicios:

     ![output docker service ps services!](/images/docker-service-ps-services.jpg "output docker service ps services")

- Ver los logs de un servicio o tarea

    Ejemplo para ver los logs del *loadbalancer*:

    ```
    docker service logs -f --tail=50 demo_loadbalancer
    ```